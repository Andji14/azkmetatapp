export const SAMPLE_ACTION = 'SAMPLE_ACTION';
export const LOADING_START = 'LOADING_START';
export const LOADING_STOP = 'LOADING_STOP';

export const sampleAction = (sample) => ({
  type: SAMPLE_ACTION,
  sample: sample
})

export const loadingStart = () => ({
  type: LOADING_START,
  loading: true
})

export const loadingStop = () => ({
  type: LOADING_STOP,
  loading: false
})
