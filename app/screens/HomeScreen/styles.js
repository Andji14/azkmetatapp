import { StyleSheet } from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Dimensions from 'Dimensions';
var { height, width } = Dimensions.get('window');

export default EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    color: 'white',
  },
  navigation: {
    backgroundColor: '$primaryBlue',
    width: '50%',
    height: 50,
    padding: 10,
    marginVertical: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
