import React, {Component} from 'react';
import { connect } from 'react-redux';
 import { Header } from '../../components';
// import Icon from 'react-native-vector-icons/FontAwesome';

import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from './styles';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

class HomeScreen extends Component {
  render(){
    return (
      <View style={styles.container}>
       <Header />

      </View>
    )
  }
}


export default HomeScreen;
