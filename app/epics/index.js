
import {Observable} from 'rxjs';
import {combineEpics} from 'redux-observable';
import {loadingStop, LOADING_START} from "../actions";

function loadingEpic(action$) {
  return action$.ofType(LOADING_START)
    .switchMap(({loading}) => {
      return Observable.of(loadingStop()).delay(2000);
    });
}

export const rootEpic = combineEpics(loadingEpic);
