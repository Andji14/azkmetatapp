import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from './styles';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.LeftContainer}>
          <View>
            <Icon name="ok" size={20} color="white" />
          </View>
        </TouchableOpacity>

        <View style={styles.centerContainer}>
          <Text >Sample Component</Text>
        </View>
        <TouchableOpacity style={styles.LeftContainer}>
          <View>
            <Icon name="ok" size={20} color="white" />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default Header;
