import React, {Component} from 'react';

import { connect } from 'react-redux';

import { sampleAction, loadingStart, loadingStop } from '../../actions';
// import Icon from 'react-native-vector-icons/FontAwesome';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from './styles';

class Home extends Component {
  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Home Component</Text>
        <TouchableOpacity onPress={() => {
          this.props.dispatch(sampleAction(!this.props.boolValue));
          // this.props.navigation.navigate('Sample');
        }} style={styles.navigation}>
          <Text style={styles.text}>Navigate to Sample</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          this.props.dispatch(loadingStart());
        }} style={styles.navigation}>
          <Text style={styles.text}>Start Loading</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          this.props.dispatch(loadingStop());
        }} style={styles.navigation}>
          <Text style={styles.text}>Stop Loading</Text>
        </TouchableOpacity>
        {this.props.loading && (
          <Text>Loading...</Text>
        )}
        <Icon name="fire" size={80} color="#bf1313" />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    boolValue: state.sample.sample,
    loading: state.sample.loading,
  };
}

export default connect(mapStateToProps)(Home);
