import React, {Component} from 'react';
import { connect } from 'react-redux';

import {
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from './styles';

class Sample extends Component {
  render(){
    return (
      <View style={styles.container}>
        <Text>Sample Component</Text>
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate('Home');
        }} style={styles.navigation}>
          <Text>Navigate to Home</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default connect()(Sample);
