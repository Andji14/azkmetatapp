import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '$background',
    justifyContent: 'center',
    alignItems: 'center'
  },
  navigation: {
    backgroundColor: '$primaryBlue',
    width: '50%',
    height: 50,
    padding: 10,
    marginVertical: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
