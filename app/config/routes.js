import { StatusBar } from 'react-native';
import { StackNavigator } from 'react-navigation';

import {
  HomeScreen
} from '../screens';

export default StackNavigator({
  HomeScreen: {
    screen: HomeScreen,
  },

}, {
    headerMode: 'none',
    cardStyle: { paddingTop: StatusBar.currentHeight },
  })
